package serverModels

// ReceiveQuestionInput is the body data and params combined into a single struct.
type ReceiveQuestionInput struct {
	GameParams
	ReceiveQuestion
}
