package serverModels

// QuestionInput is the body data and params combined into a single struct.
type QuestionInput struct {
	GameParams
	ReceiveQuestion
}
